import java.lang.CloneNotSupportedException;
import java.lang.Object;
import java.lang.Override;

public class Moschetto {

    protected int colpi;

    public Moschetto() {
        colpi=0;
    }

    public int ricarica() {
            return colpi++;
    }

    public String premi_grilletto() {
         if(colpi>0)
         {colpi--; return "Bang"; }
         return "";
    }
}